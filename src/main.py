from create_matchups import create_rs_matchups, create_tourney_matchups
from get_season_stats import calculate_advanced_stats, calculate_running_averages_off_def_stats, get_conference

men_rs_path = "./data/MRegularSeasonDetailedResults.csv"
women_rs_path = "./data/WRegularSeasonDetailedResults.csv"

men_to_path = "./data/MTourneyDetailedResults.csv"
women_to_path = "./data/WTourneyDetailedResults.csv"

calc_initial_stats = True

# Calculate the regular season stats and advanced stats

if calc_initial_stats:
    calculate_running_averages_off_def_stats(men_rs_path)
    calculate_advanced_stats(men_rs_path)
    get_conference(men_rs_path)
    create_rs_matchups(men=True, rs=True)
    create_rs_matchups(men=True, rs=False)
    create_tourney_matchups(men=True, current_year=2024, daynum=134)

    calculate_running_averages_off_def_stats(women_rs_path)
    calculate_advanced_stats(women_rs_path)
    get_conference(women_rs_path)
    create_rs_matchups(men=False, rs=True)
    create_rs_matchups(men=False, rs=False)
    create_tourney_matchups(men=False, current_year=2024, daynum=134)
