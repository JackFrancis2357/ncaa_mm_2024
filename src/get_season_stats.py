import os
from collections import defaultdict

import numpy as np
import pandas as pd


def calculate_running_averages_off_def_stats(csv_input):
    df = pd.read_csv(csv_input)
    csv_output = f'./int_data/{os.path.basename(csv_input).split(".")[0]}_int_1_season_stats.csv'

    team_stats = defaultdict(lambda: defaultdict(list))
    output_rows = []
    current_season = None

    def update_stats(team_id, prefix, row, stat_type):
        for col in df.columns:
            if col.startswith(prefix):
                stat = col[1:]  # Generalize the stat name
                stat_name = f"{stat_type}_{stat}"  # Prepend with "O_" or "D_"
                try:
                    value = float(row[col])  # Ensure value is float
                    team_stats[team_id][stat_name].append(value)
                except ValueError:
                    pass  # Ignore columns that cannot be converted to float

    for _, row in df.iterrows():
        season = row["Season"]

        # Reset stats for a new season
        if season != current_season:
            team_stats = defaultdict(lambda: defaultdict(list))
            current_season = season

        w_team = row["WTeamID"]
        l_team = row["LTeamID"]
        day_num = row["DayNum"]

        # Update offensive stats for the winning team and defensive stats for the losing team
        update_stats(w_team, "W", row, "O")
        update_stats(l_team, "L", row, "D")

        # Update defensive stats for the winning team and offensive stats for the losing team
        update_stats(w_team, "L", row, "D")
        update_stats(l_team, "W", row, "O")

        # Calculate current stats for both teams and append to output
        for team in [w_team, l_team]:
            current_stats = {"Season": season, "TeamID": team, "DayNum": day_num}
            for stat, values in team_stats[team].items():
                current_stats[stat] = np.mean(values)  # Calculate running average
            output_rows.append(current_stats)

    # Prepare the output DataFrame
    output_df = pd.DataFrame(output_rows)
    output_df = output_df.sort_values(by=["Season", "TeamID", "DayNum"]).drop_duplicates(
        subset=["Season", "TeamID", "DayNum"], keep="last"
    )

    # Round numeric columns to three decimal places
    for col in output_df.columns:
        if output_df[col].dtype == "float64":
            output_df[col] = output_df[col].round(3)

    # Save to CSV
    output_df.to_csv(csv_output, index=False)


# input_csv_path = "./data/MRegularSeasonDetailedResults.csv"
# output_csv_path = "./data/test_output.csv"
# # Rerun the corrected function
# # calculate_running_averages_off_def_stats(input_csv_path, output_csv_path)

# df = pd.read_csv(output_csv_path)


def calculate_advanced_stats(filename):
    csv_input = f'./int_data/{os.path.basename(filename).split(".")[0]}_int_1_season_stats.csv'
    df = pd.read_csv(csv_input)
    csv_output = f'./int_data/{os.path.basename(filename).split(".")[0]}_int_2_advanced_stats.csv'

    df["O_Pos"] = (
        0.5 * (df["O_FGA"])
        + 0.4 * (df["O_FTA"])
        - 1.06 * (df["O_OR"] / (df["O_OR"] + df["D_DR"])) * (df["O_FGA"] - df["O_FGM"])
        + df["D_TO"]
    )

    df["D_Pos"] = (
        0.5 * (df["D_FGA"])
        + 0.4 * (df["D_FTA"])
        - 1.06 * (df["D_OR"] / (df["D_OR"] + df["O_DR"])) * (df["D_FGA"] - df["D_FGM"])
        + df["O_TO"]
    )

    df["O_Rat"] = 100 / (df["O_Pos"] + df["D_Pos"]) * df["O_Score"]
    df["D_Rat"] = 100 / (df["O_Pos"] + df["D_Pos"]) * df["D_Score"]

    df["O_FTRat"] = df["O_FTM"] / df["O_FGA"]
    df["D_FTRat"] = df["D_FTM"] / df["D_FGA"]

    df["O_EFG"] = (df["O_FGM"] + 0.5 * df["O_FGM3"]) / df["O_FGA"]
    df["D_EFG"] = (df["D_FGM"] + 0.5 * df["D_FGM3"]) / df["D_FGA"]

    df["O_TS"] = df["O_Score"] / (2 * (df["O_FGA"] + 0.475 * df["O_FTA"]))
    df["D_TS"] = df["D_Score"] / (2 * (df["D_FGA"] + 0.475 * df["D_FTA"]))

    df["O_ARat"] = df["O_Ast"] / (df["O_FGA"] + 0.475 * df["O_FTA"] + df["O_Ast"] + df["O_TO"])
    df["D_ARat"] = df["D_Ast"] / (df["D_FGA"] + 0.475 * df["D_FTA"] + df["D_Ast"] + df["D_TO"])

    df["O_TORat"] = df["O_TO"] / (df["O_FGA"] + 0.475 * df["O_FTA"] + df["O_Ast"] + df["O_TO"])
    df["D_TORat"] = df["D_TO"] / (df["D_FGA"] + 0.475 * df["D_FTA"] + df["D_Ast"] + df["D_TO"])

    # Round numeric columns to three decimal places
    for col in df.columns:
        if df[col].dtype == "float64":
            df[col] = df[col].round(3)

    df = df.drop(["O_TeamID", "D_TeamID"], axis=1)

    df.to_csv(csv_output, index=False)


def get_conference(filename, men=True):
    csv_input = f'./int_data/{os.path.basename(filename).split(".")[0]}_int_2_advanced_stats.csv'
    df = pd.read_csv(csv_input)
    csv_output = f'./int_data/{os.path.basename(filename).split(".")[0]}_int_3_advanced_stats_conf.csv'

    if men:
        conferences_df = pd.read_csv("./data/MTeamConferences.csv")
    else:
        conferences_df = pd.read_csv("./data/WTeamConferences.csv")

    df = pd.merge(df, conferences_df, "left", left_on=["Season", "TeamID"], right_on=["Season", "TeamID"])
    df.to_csv(csv_output, index=False)
