import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder


def get_train_val_test(rs_df, to_df, men=True):
    min_men_year = 2003
    min_women_year = 2010

    valid_year = 2017
    test_year = 2021
    min_year = min_men_year if men else min_women_year

    # Combine rs_df and to_df for training data with seasons between min_year and valid_year (exclusive)
    train_df = pd.concat(
        [rs_df, to_df[(to_df["Season"] >= min_year) & (to_df["Season"] < valid_year)]], ignore_index=True
    ).reset_index(drop=True)

    # Validation data includes seasons from valid_year to test_year (exclusive)
    valid_df = to_df[(to_df["Season"] >= valid_year) & (to_df["Season"] < test_year)].reset_index(drop=True)

    # Test data includes seasons from test_year onwards
    test_df = to_df[to_df["Season"] >= test_year].reset_index(drop=True)

    return train_df, valid_df, test_df


def get_train_val_test_by_season(rs_df, to_df, men=True):
    min_men_year = 2003
    min_women_year = 2010

    valid_year = 2017
    test_year = 2021
    min_year = min_men_year if men else min_women_year

    # Combine rs_df and to_df for training data with seasons between min_year and valid_year (exclusive)
    train_df = pd.concat(
        [
            rs_df[(rs_df["Season"] >= min_year) & (rs_df["Season"] < valid_year)],
            to_df[(to_df["Season"] >= min_year) & (to_df["Season"] < valid_year)],
        ],
        ignore_index=True,
    ).reset_index(drop=True)

    # Validation data includes seasons from valid_year to test_year (exclusive)
    valid_df = pd.concat(
        [
            rs_df[(rs_df["Season"] >= valid_year) & (rs_df["Season"] < test_year)],
            to_df[(to_df["Season"] >= valid_year) & (to_df["Season"] < test_year)],
        ],
        ignore_index=True,
    ).reset_index(drop=True)

    # Test data includes seasons from test_year onwards
    test_df = pd.concat([rs_df[rs_df["Season"] >= test_year], to_df[to_df["Season"] >= test_year]]).reset_index(
        drop=True
    )

    return train_df, valid_df, test_df


def generate_weights(df, daynum_threshold=133, multiplier=10):
    """
    Generates weights for training samples based on 'Season' and 'DayNum'.

    Parameters:
    - df: DataFrame containing at least 'Season' and 'DayNum' columns.
    - daynum_threshold: The threshold of 'DayNum' above which the weight is significantly increased.
    - multiplier: The multiplier applied to weights for 'DayNum' above the threshold.

    Returns:
    - A numpy array of weights for each row in the DataFrame.
    """
    # Normalize 'Season' and 'DayNum' to have a base weight that increases linearly
    season_weight = (df["Season"] - df["Season"].min()) / (df["Season"].max() - df["Season"].min())
    daynum_weight = (df["DayNum"] - df["DayNum"].min()) / (df["DayNum"].max() - df["DayNum"].min())

    # Calculate the base weight by combining 'Season' and 'DayNum'
    base_weight = season_weight + daynum_weight

    # Apply the multiplier for 'DayNum' above the threshold
    special_weight = np.where(df["DayNum"] > daynum_threshold, multiplier, 1)

    # The final weight is a product of the base weight and the special condition multiplier
    final_weight = base_weight * special_weight

    return final_weight


def preprocess_data(train, valid, test, cur_year_games):
    """
    Applies one-hot encoding to 'T1_ConfAbbrev' and 'T2_ConfAbbrev' columns in the datasets.
    Parameters:
        train (pd.DataFrame): Training dataset.
        valid (pd.DataFrame): Validation dataset.
        test (pd.DataFrame): Test dataset.
    Returns:
        Tuple of DataFrames: (train_encoded, valid_encoded, test_encoded).
    """

    # Columns to drop
    columns_to_drop = ["Season", "DayNum", "T1_Score", "T2_Score"]

    # Columns to encode
    columns_to_encode = ["T1_ConfAbbrev", "T2_ConfAbbrev"]

    # Drop specified columns from each DataFrame
    train_dropped = train.drop(columns=columns_to_drop, errors="ignore")
    valid_dropped = valid.drop(columns=columns_to_drop, errors="ignore")
    test_dropped = test.drop(columns=columns_to_drop, errors="ignore")
    cur_year_games_dropped = cur_year_games.drop(columns=columns_to_drop, errors="ignore")

    # Setting up the ColumnTransformer with OneHotEncoder
    preprocessor = ColumnTransformer(
        transformers=[("cat", OneHotEncoder(sparse_output=False, handle_unknown="ignore"), columns_to_encode)],
        remainder="passthrough",  # Keep the rest of the columns unchanged
    )

    # Fitting the preprocessor on the train data after dropping specified columns
    preprocessor.fit(train_dropped)

    # Transforming the datasets
    train_transformed = preprocessor.transform(train_dropped)
    valid_transformed = preprocessor.transform(valid_dropped)
    test_transformed = preprocessor.transform(test_dropped)
    cur_year_transformed = preprocessor.transform(cur_year_games_dropped)

    # Getting the feature names for the transformed columns
    transformed_columns = preprocessor.named_transformers_["cat"].get_feature_names_out(columns_to_encode)

    # All column names after transformation
    remainder_columns = [col for col in train_dropped.columns if col not in columns_to_encode]
    all_columns = list(transformed_columns) + remainder_columns

    # Convert the numpy arrays back to DataFrames
    train_preprocessed = pd.DataFrame(train_transformed, columns=all_columns, index=train_dropped.index)
    valid_preprocessed = pd.DataFrame(valid_transformed, columns=all_columns, index=valid_dropped.index)
    test_preprocessed = pd.DataFrame(test_transformed, columns=all_columns, index=test_dropped.index)
    cur_year_preprocessed = pd.DataFrame(cur_year_transformed, columns=all_columns, index=cur_year_games_dropped.index)

    cur_year_preprocessed = cur_year_preprocessed.drop("Result", axis=1)

    return train_preprocessed, valid_preprocessed, test_preprocessed, cur_year_preprocessed


men_rs_df = pd.read_csv("./int_data/MRegularSeason_Matchups_Train.csv")
men_to_df = pd.read_csv("./int_data/MNCAATourney_Matchups_Train.csv")
men_new_to_df = pd.read_csv("./int_data/M2024_Tournament_Matchups.csv")
women_rs_df = pd.read_csv("./int_data/WRegularSeason_Matchups_Train.csv")
women_to_df = pd.read_csv("./int_data/WNCAATourney_Matchups_Train.csv")
women_new_to_df = pd.read_csv("./int_data/W2024_Tournament_Matchups.csv")

# Run both
my_id = "W"

if my_id == "M":
    rs_df = men_rs_df
    to_df = men_to_df
    new_to_df = men_new_to_df
else:
    rs_df = women_rs_df
    to_df = women_to_df
    new_to_df = women_new_to_df

train, val, test = get_train_val_test(rs_df, to_df)
train_weights = generate_weights(train)
train, val, test, cur_year_preprocessed = preprocess_data(train, val, test, new_to_df)

train.to_csv(f"./training_data/{my_id}_train.csv", index=False)
val.to_csv(f"./training_data/{my_id}_val.csv", index=False)
test.to_csv(f"./training_data/{my_id}_test.csv", index=False)
cur_year_preprocessed.to_csv(f"./training_data/{my_id}_cur_year.csv", index=False)
train_weights.to_csv(f'./training_data/{my_id}_train_weights.csv', header=False, index=False)

train, val, test = get_train_val_test_by_season(rs_df, to_df)
train_weights = generate_weights(train)
train, val, test, cur_year_preprocessed = preprocess_data(train, val, test, new_to_df)

train.to_csv(f"./training_data/{my_id}_train_season.csv", index=False)
val.to_csv(f"./training_data/{my_id}_val_season.csv", index=False)
test.to_csv(f"./training_data/{my_id}_test_season.csv", index=False)
cur_year_preprocessed.to_csv(f"./training_data/{my_id}_cur_year.csv", index=False)
train_weights.to_csv(f'./training_data/{my_id}_train_weights_season.csv', header=False, index=False)



# model = train_xgboost(0, train, val, test)
# get_new_preds(cur_year_preprocessed, model)
