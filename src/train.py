import matplotlib.pyplot as plt
import pandas as pd
import xgboost as xgb
from bracketeer import build_bracket
from joblib import dump, load
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV


def get_ml_train_val_test(train, val, test):
    X_train = train.drop("Result", axis=1)
    y_train = train["Result"]

    X_val = val.drop("Result", axis=1)
    y_val = val["Result"]

    X_test = test.drop("Result", axis=1)
    y_test = test["Result"]
    return X_train, y_train, X_val, y_val, X_test, y_test


def train_xgboost(train, val, test, weights):

    X_train, y_train, X_val, y_val, X_test, y_test = get_ml_train_val_test(train, val, test)

    model = xgb.XGBClassifier(
        objective="binary:logistic",  # or 'multi:softprob' for multi-class classification
        n_estimators=1000,  # Number of trees to train
        learning_rate=0.1,  # Step size shrinkage used in update to prevents overfitting
        max_depth=10,  # Maximum depth of a tree
        eval_metric="logloss",  # Evaluation metrics for validation data
        use_label_encoder=False,  # To avoid warning in newer versions of XGBoost
    )

    # Fit the model
    model.fit(
        X_train,
        y_train,
        eval_set=[(X_train, y_train), (X_val, y_val)],  # Adding validation for tracking the logloss
        early_stopping_rounds=10,  # Stop if no improvement after 10 rounds
    )

    # Predictions
    y_pred_train = model.predict(X_train)
    y_pred_val = model.predict(X_val)
    y_pred_test = model.predict(X_test)

    # Evaluate the predictions
    accuracy = accuracy_score(y_train, y_pred_train)
    print(f"Train Accuracy: {accuracy}")

    # Evaluate the predictions
    accuracy = accuracy_score(y_val, y_pred_val)
    print(f"Val Accuracy: {accuracy}")

    # Evaluate the predictions
    accuracy = accuracy_score(y_test, y_pred_test)
    print(f"Test Accuracy: {accuracy}")
    dump(model, "xgboost1.joblib")
    return model


def train_xgboost_with_weights(train, val, test, weights):

    X_train, y_train, X_val, y_val, X_test, y_test = get_ml_train_val_test(train, val, test)

    # Convert the datasets into DMatrix objects
    dtrain = xgb.DMatrix(X_train, label=y_train, weight=weights)
    dval = xgb.DMatrix(X_val, label=y_val)
    dtest = xgb.DMatrix(X_test, label=y_test)

    # Define parameters for the booster
    params = {
        "objective": "binary:logistic",
        "eval_metric": "logloss",
        "learning_rate": 0.1,
        "max_depth": 10,
        "subsample": 1.0,
        "colsample_bytree": 0.8,
        "use_label_encoder": False,
    }

    # Specify the validation set to watch performance
    evals = [(dtrain, "train"), (dval, "val")]

    # Train the model
    bst = xgb.train(
        params,
        dtrain,
        num_boost_round=1000,
        evals=evals,
        early_stopping_rounds=10,
    )

    # Predictions
    y_pred_train = bst.predict(dtrain)
    y_pred_val = bst.predict(dval)
    y_pred_test = bst.predict(dtest)

    # Convert probabilities to binary output using 0.5 as the threshold
    y_pred_train_binary = [1 if i > 0.5 else 0 for i in y_pred_train]
    y_pred_val_binary = [1 if i > 0.5 else 0 for i in y_pred_val]
    y_pred_test_binary = [1 if i > 0.5 else 0 for i in y_pred_test]

    # Evaluate the predictions
    print(f"Train Accuracy: {accuracy_score(y_train, y_pred_train_binary)}")
    print(f"Val Accuracy: {accuracy_score(y_val, y_pred_val_binary)}")
    print(f"Test Accuracy: {accuracy_score(y_test, y_pred_test_binary)}")

    # Save the model
    bst.save_model("xgboost_model.json")

    feature_importance = bst.get_score(importance_type="weight")  # You can also use 'gain' or 'cover'

    # Sort the feature importance in descending order
    sorted_importance = sorted(feature_importance.items(), key=lambda x: x[1], reverse=True)

    # # Plotting
    # plt.figure(figsize=(10, 8))
    # plt.barh([x[0] for x in sorted_importance], [x[1] for x in sorted_importance])
    # plt.xlabel("Importance")
    # plt.ylabel("Features")
    # plt.title("Feature Importance")
    # plt.gca().invert_yaxis()  # Invert the Y-axis to have the most important feature on top
    # # plt.show()

    return bst


def train_xgboost_with_hyperparameter_tuning(train, val, test):
    X_train, y_train, X_val, y_val, X_test, y_test = get_ml_train_val_test(train, val, test)

    # Initialize the XGBoost classifier
    model = xgb.XGBClassifier(
        objective="binary:logistic",
        use_label_encoder=False,  # To avoid warning in newer versions of XGBoost
        eval_metric="logloss",
    )

    # Define the parameter grid to search over
    param_grid = {
        "n_estimators": [100, 500, 1000],
        # "learning_rate": [0.01, 0.05, 0.1],
        # "max_depth": [3, 5, 7, 10],
        # "subsample": [0.8, 0.9, 1.0],
        # "colsample_bytree": [0.8, 0.9, 1.0],
    }

    # Initialize GridSearchCV
    grid_search = GridSearchCV(model, param_grid, scoring="accuracy", cv=2, verbose=2, n_jobs=-1)

    # Fit GridSearchCV
    grid_search.fit(X_train, y_train)

    # Best model
    best_model = grid_search.best_estimator_

    # Predictions
    y_pred_train = best_model.predict(X_train)
    y_pred_val = best_model.predict(X_val)
    y_pred_test = best_model.predict(X_test)

    # Evaluate the predictions
    print(f"Train Accuracy: {accuracy_score(y_train, y_pred_train)}")
    print(f"Val Accuracy: {accuracy_score(y_val, y_pred_val)}")
    print(f"Test Accuracy: {accuracy_score(y_test, y_pred_test)}")

    print(grid_search.best_params_)
    dump(grid_search.best_estimator_, "best_model.joblib")

    return best_model


def train_xgboost_weights_with_hyperparameter_tuning(train, val, test, weights):
    X_train, y_train, X_val, y_val, _, _ = get_ml_train_val_test(train, val, val)  # Just for simplicity

    dtrain = xgb.DMatrix(X_train, label=y_train, weight=weights)
    dval = xgb.DMatrix(X_val, label=y_val)

    # Example parameter grid (you should expand this with more parameters and values)
    param_grid = {
        "learning_rate": [0.01, 0.1, 0.2],
        "max_depth": [6, 10, 15],
        "subsample": [0.8, 1.0],
        "colsample_bytree": [0.8, 1.0],
        "num_boost_rounds": [100],
    }

    # Placeholder for best score and parameters
    min_logloss = float("Inf")
    best_params = None

    # Loop over the grid
    for learning_rate in param_grid["learning_rate"]:
        for max_depth in param_grid["max_depth"]:
            for subsample in param_grid["subsample"]:
                for colsample_bytree in param_grid["colsample_bytree"]:
                    for num_boost_round in param_grid["num_boost_rounds"]:
                        print(
                            f"CV with learning_rate={learning_rate}, max_depth={max_depth}, subsample={subsample}, colsample_bytree={colsample_bytree}"
                        )

                        # Update our parameters
                        params = {
                            "learning_rate": learning_rate,
                            "max_depth": max_depth,
                            "subsample": subsample,
                            "colsample_bytree": colsample_bytree,
                            "objective": "binary:logistic",
                            "eval_metric": "logloss",
                        }

                        # Cross-validation
                        cv_results = xgb.cv(
                            params,
                            dtrain,
                            num_boost_round=num_boost_round,
                            seed=42,
                            nfold=2,
                            metrics={"logloss"},
                            early_stopping_rounds=20,
                        )

                        # Update best MAE
                        mean_logloss = cv_results["test-logloss-mean"].min()
                        boost_rounds = cv_results["test-logloss-mean"].argmin()
                        print(f"\tlogloss {mean_logloss} for {boost_rounds} rounds")
                        if mean_logloss < min_logloss:
                            min_logloss = mean_logloss
                            best_params = (learning_rate, max_depth, subsample, colsample_bytree)

    print(
        f"Best params: {best_params[0]}, {best_params[1]}, {best_params[2]}, {best_params[3]}, logloss: {min_logloss}"
    )

    # Train final model with the best parameters
    best_params_dict = {
        "learning_rate": best_params[0],
        "max_depth": best_params[1],
        "subsample": best_params[2],
        "colsample_bytree": best_params[3],
        "objective": "binary:logistic",
        "eval_metric": "logloss",
        "use_label_encoder": False,
    }
    final_model = xgb.train(best_params_dict, dtrain, num_boost_round=boost_rounds)

    # Save the final model
    final_model.save_model("xgboost_model_optimized.json")

    return final_model


def get_new_preds(year_data, model, identifier, year=2024, men=True):
    model = xgb.XGBClassifier()
    model = load("best_model.joblib")
    df = pd.DataFrame(index=range(3000), columns=["ID", "Pred"])
    # submission_file = pd.read_csv("./data/SampleSubmission2024.csv", index_col=0)
    # submission_filename = "final_submission.csv"
    y_pred = model.predict_proba(year_data)
    num_games = y_pred.shape[0] // 2
    for i in range(num_games):
        team_1 = int(year_data.loc[2 * i, "Team2ID"])
        team_2 = int(year_data.loc[2 * i, "Team1ID"])
        team_1_prob = (y_pred[2 * i] + (1 - y_pred[2 * i + 1])) / 2
        team_2_prob = ((1 - y_pred[2 * i]) + y_pred[2 * i + 1]) / 2
        if team_1 < team_2:
            sub_id = f"{year}_{team_1}_{team_2}"
            # submission_file.loc[sub_id, :] = team_1_prob[0]
            df.iloc[i, :] = sub_id, team_1_prob[0]
        else:
            sub_id = f"{year}_{team_2}_{team_1}"
            # submission_file.loc[sub_id, :] = team_2_prob[0]
            df.iloc[i, :] = sub_id, team_2_prob[0]
    df.to_csv(f"old_kaggle_submission_format_{identifier}.csv")
    # submission_file.to_csv(submission_filename)
    b = build_bracket(
        outputPath=f"output_{identifier}.png",
        teamsPath=f"./data/{identifier}Teams.csv",
        seedsPath=f"./data/{identifier}NCAATourneySeeds.csv",
        submissionPath=f"old_kaggle_submission_format_{identifier}.csv",
        slotsPath=f"./data/{identifier}NCAATourneySlots.csv",
        year=2024,
    )


def get_new_preds_xgboost(year_data, model, identifier, year=2024, men=True):
    model = xgb.Booster()
    model.load_model("xgboost_model.json")
    # raw_year_data = year_data.copy(deep=True)
    # year_data = year_data.drop(columns=["Team1ID", "Team2ID"], errors="ignore")

    # model.load_model("xgboost_model_optimized.json")
    df = pd.DataFrame(index=range(3000), columns=["ID", "Pred"])
    # submission_file = pd.read_csv("./data/SampleSubmission2024.csv", index_col=0)
    # submission_filename = "final_submission.csv"
    dyear_data = xgb.DMatrix(year_data)
    y_pred = model.predict(dyear_data)
    num_games = y_pred.shape[0] // 2
    for i in range(num_games):
        team_1 = int(year_data.loc[2 * i, "Team1ID"])
        team_2 = int(year_data.loc[2 * i, "Team2ID"])
        team_1_prob = (y_pred[2 * i] + (1 - y_pred[2 * i + 1])) / 2
        team_2_prob = ((1 - y_pred[2 * i]) + y_pred[2 * i + 1]) / 2
        if team_1 < team_2:
            sub_id = f"{year}_{team_1}_{team_2}"
            # submission_file.loc[sub_id, :] = team_1_prob[0]
            df.iloc[i, :] = sub_id, team_1_prob
        else:
            sub_id = f"{year}_{team_2}_{team_1}"
            # submission_file.loc[sub_id, :] = team_2_prob[0]
            df.iloc[i, :] = sub_id, team_2_prob
    df.to_csv(f"old_kaggle_submission_format_{identifier}.csv")
    # submission_file.to_csv(submission_filename)
    b = build_bracket(
        outputPath=f"output_{identifier}.png",
        teamsPath=f"./data/{identifier}Teams.csv",
        seedsPath=f"./data/{identifier}NCAATourneySeeds.csv",
        submissionPath=f"old_kaggle_submission_format_{identifier}.csv",
        slotsPath=f"./data/{identifier}NCAATourneySlots.csv",
        year=2024,
    )


my_id = "W"
# my_season_id = "_season"
my_season_id = ""
train = pd.read_csv(f"./training_data/{my_id}_train{my_season_id}.csv")
val = pd.read_csv(f"./training_data/{my_id}_val{my_season_id}.csv")
test = pd.read_csv(f"./training_data/{my_id}_test{my_season_id}.csv")
cur_year = pd.read_csv(f"./training_data/{my_id}_cur_year.csv")
weights = pd.read_csv(f"./training_data/{my_id}_train_weights{my_season_id}.csv", header=None)

columns_to_drop = ["Team1ID", "Team2ID"]
# train = train.drop(columns=columns_to_drop, errors="ignore")
# val = val.drop(columns=columns_to_drop, errors="ignore")
# test = test.drop(columns=columns_to_drop, errors="ignore")
# cur_year = cur_year.drop(columns=columns_to_drop, errors="ignore")

model = train_xgboost_with_weights(train, val, test, weights)

get_new_preds_xgboost(cur_year, model, my_id)

# train_xgboost_weights_with_hyperparameter_tuning(train, val, test, weights)
