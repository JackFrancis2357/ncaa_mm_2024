import itertools

import numpy as np
import pandas as pd


def create_rs_matchups(men=True, rs=True):
    if men:
        gen_id = "M"
    else:
        gen_id = "W"
    if rs:
        tourn_id = "RegularSeason"
    else:
        tourn_id = "NCAATourney"
    df = pd.read_csv(f"./data/{gen_id}{tourn_id}DetailedResults.csv")
    stats_df = pd.read_csv(f"./int_data/{gen_id}RegularSeasonDetailedResults_int_3_advanced_stats_conf.csv")
    output_csv = f"./int_data/{gen_id}{tourn_id}_Matchups_Train.csv"

    info_cols = ["Season", "DayNum", "Team1ID", "Team2ID"]
    my_cols = np.array(stats_df.columns[3:])
    t1_cols = [f"T1_{col}" for col in my_cols]
    t2_cols = [f"T2_{col}" for col in my_cols]
    result_cols = ["T1_Score", "T2_Score", "Result"]
    all_cols = info_cols + t1_cols + t2_cols + result_cols
    all_rows = []

    for _, row in df.iterrows():
        winner_stats = stats_df[stats_df["Season"] == row["Season"]]
        winner_stats = winner_stats[winner_stats["TeamID"] == row["WTeamID"]]
        winner_stats = winner_stats[winner_stats["DayNum"] < row["DayNum"]]
        loser_stats = stats_df[stats_df["Season"] == row["Season"]]
        loser_stats = loser_stats[loser_stats["TeamID"] == row["LTeamID"]]
        loser_stats = loser_stats[loser_stats["DayNum"] < row["DayNum"]]
        if len(winner_stats) < 1 or len(loser_stats) < 1:
            continue

        winner_results = winner_stats.iloc[-1, :]
        loser_results = loser_stats.iloc[-1, :]

        t1 = np.array(winner_results[3:])
        t2 = np.array(loser_results[3:])
        t1_s = np.array(row["WScore"]).reshape(1)
        t2_s = np.array(row["LScore"]).reshape(1)
        t1_r = np.array(1).reshape(1)
        t2_r = np.array(0).reshape(1)
        s = np.array(row["Season"]).reshape(1)
        dn = np.array(row["DayNum"]).reshape(1)
        t1_id = np.array(row["WTeamID"]).reshape(1)
        t2_id = np.array(row["LTeamID"]).reshape(1)
        r1 = np.concatenate((s, dn, t1_id, t2_id, t1, t2, t1_s, t2_s, t1_r))
        r2 = np.concatenate((s, dn, t2_id, t1_id, t2, t1, t2_s, t1_s, t2_r))
        all_rows.append(r1)
        all_rows.append(r2)

    df = pd.DataFrame(all_rows, columns=all_cols)
    df.to_csv(output_csv, index=False)


def create_tourney_matchups(men=True, current_year=2024, daynum=134):
    identifier = "M" if men else "W"
    seed_df = pd.read_csv(f"./data/{identifier}NCAATourneySeeds.csv")
    seed_df = seed_df[seed_df["Season"] == current_year]
    all_teams = seed_df["TeamID"].values
    team_pairs = [*itertools.combinations(all_teams, 2)]
    if men:
        gen_id = "M"
    else:
        gen_id = "W"

    stats_df = pd.read_csv(f"./int_data/{gen_id}RegularSeasonDetailedResults_int_3_advanced_stats_conf.csv")
    output_csv = f"./int_data/{gen_id}2024_Tournament_Matchups.csv"

    info_cols = ["Season", "DayNum", "Team1ID", "Team2ID"]
    my_cols = np.array(stats_df.columns[3:])
    t1_cols = [f"T1_{col}" for col in my_cols]
    t2_cols = [f"T2_{col}" for col in my_cols]
    result_cols = ["Result"]
    all_cols = info_cols + t1_cols + t2_cols + result_cols
    all_rows = []

    for team_1, team_2 in team_pairs:
        winner_stats = stats_df[stats_df["Season"] == current_year]
        winner_stats = winner_stats[winner_stats["TeamID"] == team_1]
        winner_stats = winner_stats[winner_stats["DayNum"] < daynum]
        loser_stats = stats_df[stats_df["Season"] == current_year]
        loser_stats = loser_stats[loser_stats["TeamID"] == team_2]
        loser_stats = loser_stats[loser_stats["DayNum"] < daynum]
        if len(winner_stats) < 1 or len(loser_stats) < 1:
            continue

        winner_results = winner_stats.iloc[-1, :]
        loser_results = loser_stats.iloc[-1, :]

        t1 = np.array(winner_results[3:])
        t2 = np.array(loser_results[3:])
        t1_r = np.array(1).reshape(1)
        t2_r = np.array(0).reshape(1)
        s = np.array(current_year).reshape(1)
        dn = np.array(daynum).reshape(1)
        t1_id = np.array(team_1).reshape(1)
        t2_id = np.array(team_2).reshape(1)
        r1 = np.concatenate((s, dn, t1_id, t2_id, t1, t2, t1_r))
        r2 = np.concatenate((s, dn, t2_id, t1_id, t2, t1, t2_r))
        all_rows.append(r1)
        all_rows.append(r2)

    df = pd.DataFrame(all_rows, columns=all_cols)
    df.to_csv(output_csv, index=False)

create_tourney_matchups(men=False, current_year=2024, daynum=134)
create_tourney_matchups(men=True, current_year=2024, daynum=134)
